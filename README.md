# morse-code

Text to morse code app

#### [https://pragalakis.gitlab.io/morse-code/](https://pragalakis.gitlab.io/morse-code/)


## License

MIT , see [LICENSE](LICENSE) for details.

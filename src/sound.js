class Sound {
  constructor(context) {
    this.context = context;
  }

  init() {
    this.oscillator = this.context.createOscillator();
    this.gainNode = this.context.createGain();
    this.oscillator.connect(this.gainNode);
    this.gainNode.connect(this.context.destination);
    this.oscillator.type = 'Triangle';
  }

  play(value, time, type) {
    this.init();
    this.oscillator.frequency.value = value;
    this.oscillator.start(time);
    this.stop(time, type);
  }

  stop(time, type) {
    if (type === 'dot') {
      this.oscillator.stop(time + 0.1);
    } else {
      this.oscillator.stop(time + 0.3);
    }
  }
}

module.exports = Sound;

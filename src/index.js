const html = require('choo/html');
const choo = require('choo');
const css = require('sheetify');
const morseCode = require('./morse.js');
const Sound = require('./sound.js');

const app = choo();
app.use(textStore);
app.use(textStore);
app.route('/', mainView);
app.route('/morse-code', mainView);
app.mount('body');

function textStore(state, emitter) {
  state.morseText = '';
  state.alphabetText = 'Press any key to get the Morse code';
  emitter.on('add', text => {
    const textToMorse = morseCode[text];
    if (textToMorse) {
      state.morseText = textToMorse;
      state.alphabetText = Object.keys(morseCode)
        .find(key => morseCode[key] === textToMorse)
        .toUpperCase();
      emitter.emit('render');
    }
  });
}

let context;
let note;
function mainView(state, emit) {
  emit('DOMTitleChange', 'Text to Morse');

  const handleOnLoad = () => {
    context = new (window.AudioContext || window.webkitAudioContext)();
    note = new Sound(context);
  };

  const handleOnKeyDown = e => {
    emit('add', e.key.toLowerCase());
  };

  const handleOnKeyUp = e => {
    const now = context.currentTime;
    const frequency = 700;
    const textToMorse = morseCode[e.key.toLowerCase()];
    if (textToMorse) {
      textToMorse.split('').forEach((morse, i) => {
        if (morse === '.') {
          note.play(frequency, now + i / 2.5, 'dot');
        } else if (morse === '-') {
          note.play(frequency, now + i / 2.5, 'dash');
        }
      });
    }
  };

  return html`
    <body
      class=${prefix}
      onload=${handleOnLoad}
      onkeydown=${handleOnKeyDown}
      onkeyup=${handleOnKeyUp}
    >
      <div class="morse">
        ${state.morseText}
      </div>
      <div class="alphabet">
        ${state.alphabetText}
      </div>
      <footer>
        <h5>
          Made with <a href="https://choo.io/">choo</a> & ❤.
          Morse Code is <a href="https://www.gitlab.com/pragalakis/morse-code">open-source</a>
        </h5>
      </footer>
    </body>
  `;
}

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-serif;
    background-color: #ffe5e5;
    font-size: 16px;
    word-wrap: anywhere;
    height: 100vh;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    margin: 0;
  }

  :host .morse {
    color: #000;
    font-size: 12rem;
  }

  :host .alphabet {
    color: #f77;
    font-size: 2rem;
  }

  :host footer {
    position: fixed;
    bottom: 0;
  }
`;
